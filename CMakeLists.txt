cmake_minimum_required(VERSION 2.8.3)
project(hsr_2018)


find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  geometry_msgs
  actionlib
  
)


add_message_files(
  FILES
    moveit_state_msg.msg
    moveit_cmd_msg.msg
    main_cmd_msg.msg
    BoundingBox_XYZ.msg
    BoundingBoxes_XYZ.msg
)



generate_messages(
  DEPENDENCIES
    geometry_msgs
    std_msgs
)



include_directories(

  ${catkin_INCLUDE_DIRS}
)
#add_executable(main src/main.cpp)
#target_link_libraries(main ${catkin_LIBRARIES})

add_executable(simple_navigation_goals src/simple_navigation_goals.cpp)
target_link_libraries(simple_navigation_goals ${catkin_LIBRARIES})

add_executable(main2 src/main2.cpp)
target_link_libraries(main2 ${catkin_LIBRARIES})
