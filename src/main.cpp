#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Int64.h"
#include "std_msgs/Float64.h"
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Transform.h>
#include <sstream>
#include <xy_to_xyz_msgs/BoundingBox_XYZ.h>
#include <xy_to_xyz_msgs/BoundingBoxes_XYZ.h>

// #define 
// self.depth_pick = rospy.Subscriber('/pick',Transform,self.callback_pick)
//     self.depth_place = rospy.Subscriber('/place',Transform,self.callback_place)
//     self.depth_move_base_only = rospy.Subscriber('/move_base_only',Pose,self.callback_move_base_only)

void hsr_nav_goal_status_Callback(const std_msgs::Int64& msg);
void nav_goal_send(geometry_msgs::Pose A);
void Nav_status_Dis();
void NAV_Goal_send(geometry_msgs::Pose A);
void ImageCallback(const xy_to_xyz_msgs::BoundingBoxes_XYZ::ConstPtr& set);

ros::Publisher hsrb_nav_goal_pub;
ros::Publisher pick_pub;

ros::Subscriber hsrb_nav_goal_status_sub, hsrb_image_pose;

geometry_msgs::Pose nav_goal_data;
geometry_msgs::Transform point;
int finish_navi = 0;
int state_navi = 0;
int check_navi = 0;
int seq=0;
int nodeFree = 0;
int m_state = 0;

xy_to_xyz_msgs::BoundingBoxes_XYZ img;
int check_inverse = 0;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "main");


  ros::NodeHandle n;

 
  // //Publisher
  hsrb_nav_goal_pub = n.advertise<geometry_msgs::Pose>("/nav_goal", 1000);
  pick_pub=n.advertise<geometry_msgs::Transform>("/pick",1);


  // //Subscriber
  hsrb_nav_goal_status_sub = n.subscribe("/navi_finish", 1000, hsr_nav_goal_status_Callback);


  //Publisher
  hsrb_nav_goal_pub = n.advertise<geometry_msgs::Pose>("/hsr_nav_goal", 1000);
  //Subscriber
  hsrb_nav_goal_status_sub = n.subscribe("/hsr_nav_goal_status", 1000, hsr_nav_goal_status_Callback);
  hsrb_image_pose = n.subscribe("/pub_to_main/object_detection_list", 1000, &ImageCallback);
  ros::Rate loop_rate(10);


  geometry_msgs::Pose M;
  M.position.x = 2.5;
  M.position.y = -0.5;
  M.orientation.z = 0.0;
  M.orientation.w = 1.0;

  geometry_msgs::Pose N;
  N.position.x = 3.0;
  N.position.y = 0.0;
  N.orientation.z = 0.707;
  N.orientation.w = 0.707;

  geometry_msgs::Pose O;
  O.position.x = 0.0;
  O.position.y = 0.0;
  O.orientation.z = 0.0;
  O.orientation.w = 1.0;
  
  

  while (ros::ok())
  {

    ROS_INFO("%d aa",m_state);
    switch(m_state)
    {
      case 0:
      {
        ros::Duration(5).sleep();
        nav_goal_send(M);
        m_state++;
        break; 

      }
      case 1://take care
      {
        ros::Duration(5).sleep();
        nav_goal_send(N);
        m_state++;
        break;
      }
      case 2: //navi kitchen 
      {
        ros::Duration(5).sleep();
        nav_goal_send(O);
        m_state++;
        break;
      }

    }
    ros::spinOnce();

    loop_rate.sleep();
  }


  return 0;
}


void nav_goal_send(geometry_msgs::Pose A)
{
  nav_goal_data.position.x = A.position.x;
  nav_goal_data.position.y = A.position.y;
  nav_goal_data.orientation.x = A.orientation.x;
  nav_goal_data.orientation.y = A.orientation.y;
  nav_goal_data.orientation.z = A.orientation.z;
  nav_goal_data.orientation.w = A.orientation.w;
  state_navi = 81;
  check_navi = 82;
  while(state_navi<82)
  {  
    // printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 81 :{
        if(check_navi == 82)
        {
            hsrb_nav_goal_pub.publish(nav_goal_data); 
            finish_navi =0;
            check_navi = 83;
        }
        if(finish_navi == 1 && check_navi == 83)
        {
            ros::Duration(5).sleep(); 
            state_navi = 82;
            check_navi = 84;
            break;
        }
      }
      case 82 :{

        break;
      }    
    }
   ros::spinOnce(); 
  }
}

void hsr_nav_goal_status_Callback(const std_msgs::Int64& msg)
{
  finish_navi = msg.data;
}
void ImageCallback(const xy_to_xyz_msgs::BoundingBoxes_XYZ::ConstPtr& set)
{
  try
  {

    img=*set;

    if (check_inverse != 1)
      for(uint8_t i=0; i< img.boundingBoxes_xyz.size(); i++)
      {
       if(img.boundingBoxes_xyz[i].Class == "bottle" && img.boundingBoxes_xyz[i].probability > 0.35 )
        {
          point.translation.x = img.boundingBoxes_xyz[i].x;
          point.translation.y = img.boundingBoxes_xyz[i].y;
          point.translation.z = img.boundingBoxes_xyz[i].z;
          ROS_INFO("Found it");
          // image_true=1;
           //break;
        }
      }
      ROS_INFO("asd");
  }
  catch(int x)
  {
    ROS_INFO("CB ERROR");
    // continue;
  }
}