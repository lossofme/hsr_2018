#include <sstream>

#include "ros/ros.h"

#include "std_msgs/String.h"
#include "std_msgs/Int64.h"
#include "std_msgs/Float64.h"
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Transform.h>

#include <hsr_2018/BoundingBox_XYZ.h>
#include <hsr_2018/BoundingBoxes_XYZ.h>

#include <hsr_2018/moveit_state_msg.h>
#include <hsr_2018/moveit_cmd_msg.h>
#include <hsr_2018/main_cmd_msg.h>

#define STATE_MOVEIT_ERROR -1
#define STATE_MOVEIT_WAIT 0
#define STATE_MOVEIT_READY 1
#define STATE_MOVEIT_PICKING 2
#define STATE_MOVEIT_PLACING 3
#define STATE_MOVEIT_MOVING 4


#define STATE_NAVI_ERROR -1
#define STATE_NAVI_WAIT 0
#define STATE_NAVI_READY 1
#define STATE_NAVI_GOING 2
// function prototype
void navi_callback(const std_msgs::Int64& msg);

void navi_send_cmd(double x,double y,double z,double a,double b,double c,double d);

void ImageCallback(const hsr_2018::BoundingBoxes_XYZ::ConstPtr& set);

void moveit_callback(const hsr_2018::moveit_state_msg::ConstPtr& msg);

void main_cmd_callback(const hsr_2018::main_cmd_msg::ConstPtr& msg);

void moviet_send_cmd (int cmd ,double x,double y,double z,double a,double b,double c,double d);

void print_state();
// Global data
// pub
ros::Publisher hsrb_nav_goal_pub; //send position to navi
ros::Publisher moveit_pub;          //send position to pick
//sub
ros::Subscriber hsrb_nav_goal_status_sub;
ros::Subscriber hsrb_image_pose;
ros::Subscriber state_moveit_sub;
ros::Subscriber cmd_main_sub;
//data


geometry_msgs::Transform point_image;

hsr_2018::BoundingBoxes_XYZ img;

int cmd_main = 0 ;

int MOVEIT_STATE 	=	STATE_MOVEIT_WAIT;
int NAVI_STATE		=	STATE_NAVI_WAIT;
int IMAGE_STATE		=0;
bool fn_image_callback =true ; 


int main(int argc, char **argv)
{
	
	ros::init(argc, argv, "main");


	ros::NodeHandle n;

	//Publisher
	hsrb_nav_goal_pub = n.advertise<geometry_msgs::Pose>("/hsr_nav_goal", 1);   //send position to navi
	moveit_pub=n.advertise<hsr_2018::moveit_cmd_msg>("/main/moveit",1);

	//Subscriber
	hsrb_nav_goal_status_sub = n.subscribe("/hsr_nav_goal_status", 1, &navi_callback);
	hsrb_image_pose = n.subscribe("/pub_to_main/object_detection_list", 1000, &ImageCallback);
	
	state_moveit_sub = n.subscribe("/main/moveit/state", 1, &moveit_callback);
	cmd_main_sub = n.subscribe("/main/moveit/cmd", 100, &main_cmd_callback);

	// frequency loop
	ros::Rate loop_rate(5);
	
	// ros::Duration(5).sleep();
	while (ros::ok())
	{
		print_state();
		
		switch (cmd_main) 
		{

			case 1: navi_send_cmd(0.5,0,0,	0,0,0,1);
					cmd_main++;
					break;
			case 2:	if (NAVI_STATE==STATE_NAVI_READY)	
						cmd_main++;
					break;
			case 3:	moviet_send_cmd(STATE_MOVEIT_PICKING,	point_image.translation.x,point_image.translation.y,point_image.translation.z,	0.0, 0.0, 0.0, 1);	
					cmd_main++;
					break;

			case 4:	if (MOVEIT_STATE==STATE_MOVEIT_READY)	
						cmd_main++;
					break;

			case 5: navi_send_cmd(0,0,0,	0,0,0,1);
					cmd_main++;
					break;

			
		}
 
	


	ros::spinOnce();
	loop_rate.sleep();
	}
	return 0;
}


void navi_send_cmd(double x,double y,double z,double a,double b,double c,double d)
{

	geometry_msgs::Pose data_pub;
	data_pub.position.x=x;
	data_pub.position.y=y;
	data_pub.position.z=z;
	data_pub.orientation.x=a;
	data_pub.orientation.y=b;
	data_pub.orientation.z=c;
	data_pub.orientation.w=d;
	hsrb_nav_goal_pub.publish(data_pub);
	ROS_INFO("navi_send_cmd");

}

void navi_callback(const std_msgs::Int64& msg)
{
	ROS_INFO("navi_callback");
  	NAVI_STATE=msg.data;
}
void moveit_callback(const hsr_2018::moveit_state_msg::ConstPtr& msg)
{
	// ROS_INFO("moveit_callback");
	MOVEIT_STATE=msg->data;
	// ROS_INFO("%d",moveit_state);
	// cmd_main =moveit_state;
}
void main_cmd_callback(const hsr_2018::main_cmd_msg::ConstPtr& msg)
{
	ROS_INFO("MAIN_CMD_CALLBACK");
	cmd_main=msg->cmd;
}
void print_state()
{
	if (MOVEIT_STATE==STATE_MOVEIT_WAIT)
		ROS_INFO ("STATE_MOVEIT_WAIT");

	else if (MOVEIT_STATE==STATE_MOVEIT_READY)
		ROS_INFO ("STATE_MOVEIT_READY");

	else if (MOVEIT_STATE==STATE_MOVEIT_PICKING)
		ROS_INFO ("STATE_MOVEIT_PICKING");

	else if (MOVEIT_STATE==STATE_MOVEIT_PLACING)
		ROS_INFO ("STATE_MOVEIT_PLACING");

	else if (MOVEIT_STATE==STATE_MOVEIT_MOVING)
		ROS_INFO ("STATE_MOVEIT_MOVING");

	else 
		ROS_INFO ("STATE_MOVEIT_ERROR");
	ROS_INFO ("%d",NAVI_STATE);
}
void moviet_send_cmd (int cmd ,double x,double y,double z,double a,double b,double c,double d)
{
	hsr_2018::moveit_cmd_msg data_moveit_pub;
	data_moveit_pub.cmd = cmd;
	data_moveit_pub.tran.translation.x = x;
	data_moveit_pub.tran.translation.y = y;
	data_moveit_pub.tran.translation.z = z;
	data_moveit_pub.tran.rotation.x = a;
	data_moveit_pub.tran.rotation.y = b;
	data_moveit_pub.tran.rotation.z = c;
	data_moveit_pub.tran.rotation.w = d;
	moveit_pub.publish(data_moveit_pub);
	
	MOVEIT_STATE =STATE_MOVEIT_WAIT;

}
void ImageCallback(const hsr_2018::BoundingBoxes_XYZ::ConstPtr& set)
{
	ROS_INFO("ImageCallback");
	if (fn_image_callback)
	{
		try
		{
			img=*set;

		   for(uint8_t i=0; i< img.boundingBoxes_xyz.size(); i++)
		   {
				if(img.boundingBoxes_xyz[i].Class == "bottle" && img.boundingBoxes_xyz[i].probability > 0.35 )
			 	{
			   		point_image.translation.x = img.boundingBoxes_xyz[i].x;
					point_image.translation.y = img.boundingBoxes_xyz[i].y;
					point_image.translation.z = img.boundingBoxes_xyz[i].z;
					ROS_INFO("Found bottle");
			 	}
			}
	  	}
		catch(int x)
	  	{
	 		ROS_INFO("ImageCallback ERROR");
		}
	}
}


// switch (cmd_main) 
// 		{
// 			case 2: moviet_send_cmd(STATE_MOVEIT_MOVING, 	0.9,-0.9,0.0,	0.0,0.0,-0.383,0.924);
// 					cmd_main++;
// 					break;

// 			case 3:	if (MOVEIT_STATE==STATE_MOVEIT_READY)
// 						cmd_main++;
// 					break;

// 			case 4:	moviet_send_cmd(STATE_MOVEIT_PICKING,	1.4,-1.4,0.7,	0.0,0.0,-0.383,0.924);	
// 					cmd_main++;
// 					break;

// 			case 5:	if (MOVEIT_STATE==STATE_MOVEIT_READY)	
// 						cmd_main++;
// 					break;

// 			case 6: moviet_send_cmd(STATE_MOVEIT_MOVING,	0.9,-0.9,0.0,	0.0,0.0,-0.383,0.924);
// 					cmd_main++;
// 					break;

// 			case 7:	if (MOVEIT_STATE==STATE_MOVEIT_READY)	
// 						cmd_main++;
// 					break;

// 			case 8:	moviet_send_cmd(STATE_MOVEIT_PLACING,	1.4,-1.4,0.7,	0.0,0.0,-0.383,0.924);
// 					cmd_main++;

// 			case 9:	if (MOVEIT_STATE==STATE_MOVEIT_READY)	
// 						cmd_main++;
// 					break;

// 			case 10: moviet_send_cmd(STATE_MOVEIT_MOVING,	0.9,-0.9,0.0,	0.0,0.0,-0.383,0.924);
// 					cmd_main++;
// 					break;		
// 		}