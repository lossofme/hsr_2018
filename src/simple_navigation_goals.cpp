// #include <ros/ros.h>
// #include <move_base_msgs/MoveBaseAction.h>
// #include <actionlib/client/simple_action_client.h>
// #include "std_msgs/Int64.h"
// #include <geometry_msgs/Pose.h>


// typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

// move_base_msgs::MoveBaseGoal goal;
// std_msgs::Int64 nav_feedback;
// ros::Subscriber hsr_nav_goal_sub;
// ros::Publisher hsr_nav_goal_status_pub;
// void Mov_base_Callback(const geometry_msgs::Pose& nav_goal_msg);

// int main(int argc, char** argv){
//       ros::init(argc, argv, "simple_navigation_goals");
//       ros::NodeHandle n;
//       //Publisher
//       hsr_nav_goal_status_pub = n.advertise<std_msgs::Int64>("hsr_nav_goal_status", 1000);


//       //Subscriber
//       hsr_nav_goal_sub = n.subscribe("hsr_nav_goal", 1000, Mov_base_Callback);
//        // ros::Rate loop_rate(10);
     
//        ros::spin();
//       // while (ros::ok()){
//       //    ros::spinOnce();
//       //    loop_rate.sleep();

//       // }

//       return 0;
// }


// void Mov_base_Callback(const geometry_msgs::Pose& nav_goal_msg){
//             // // //data recieve feedback
//             // nav_feedback.data = 2;
//             // hsr_nav_goal_status_pub.publish(nav_feedback);

//             //tell the action client that we want to spin a thread by default
//             MoveBaseClient ac("move_base", true);

//             //wait for the action server to come up
//             while(!ac.waitForServer(ros::Duration(5.0))){
//                   ROS_INFO("Waiting for the move_base action server to come up");
//             }

           


//             if(goal.target_pose.pose.position.x != nav_goal_msg.position.x && 
//               goal.target_pose.pose.position.y != nav_goal_msg.position.y && 
//               goal.target_pose.pose.orientation.w != nav_goal_msg.orientation.w )
//             {
//                     goal.target_pose.header.frame_id = "map";
//                     goal.target_pose.header.stamp = ros::Time::now();

//                     goal.target_pose.pose.position.x = nav_goal_msg.position.x;
//                     goal.target_pose.pose.position.y = nav_goal_msg.position.y;
//                     goal.target_pose.pose.orientation.w = nav_goal_msg.orientation.w;

//                     ROS_INFO("Sending goal");
//                     ac.sendGoal(goal);

//                     ac.waitForResult();


//                     if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
//                           ROS_INFO("Robot reach the target!!!");
//                           nav_feedback.data = 1;
//                           hsr_nav_goal_status_pub.publish(nav_feedback);
//                     }
//                     else{
//                           ROS_INFO("The base failed to move to reach target");
//                           nav_feedback.data = -1;
//                           hsr_nav_goal_status_pub.publish(nav_feedback);
//                     }

//             }
//             else{ //nav target and robot position are same!!!

//                     ROS_INFO("nav target and robot position are same!!!");
//                     nav_feedback.data = -2;
//                     hsr_nav_goal_status_pub.publish(nav_feedback);
                    

//             }            
// }


#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <std_msgs/Int64.h>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
ros::Publisher status_navi;
move_base_msgs::MoveBaseGoal goal;
std_msgs::Int64 finish;


void NavCallback(const geometry_msgs::Pose& msg)
{
  ROS_INFO("CALLBACK");
  MoveBaseClient ac("move_base", true);

  //wait for the action server to come up
  while(!ac.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the move_base action server to come up");
  }

  //we'll send a goal to the robot to move 1 meter forward
  goal.target_pose.header.frame_id = "map";
  goal.target_pose.header.stamp = ros::Time::now();

  goal.target_pose.pose.position.x = msg.position.x;
  goal.target_pose.pose.position.y = msg.position.y;
  // goal.target_pose.pose.orientation.x = msg.orientation.x;
  // goal.target_pose.pose.orientation.y = msg.orientation.y;
  goal.target_pose.pose.orientation.z = msg.orientation.z;
  goal.target_pose.pose.orientation.w = msg.orientation.w;

  ROS_INFO("Sending goal");
  ac.sendGoal(goal);

  ac.waitForResult();

  if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
  {
      ROS_INFO("FINISH NAVIGATION");
      finish.data = 1;
      if(finish.data ==1)
      {
        status_navi.publish(finish);
        finish.data = 0;
      }
  }
  else
  {
    ROS_INFO("The base failed to move forward 1 meter for some reason");
    finish.data = -1;
    status_navi.publish(finish);
  }

}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "simple_navigation_goals");
  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("/hsr_nav_goal", 1000, &NavCallback);
  status_navi = n.advertise<std_msgs::Int64>("/hsr_nav_goal_status", 1000);
  //tell the action client that we want to spin a thread by default

  ros::spin();
  return 0;
}









