#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Int64.h"
#include <geometry_msgs/Pose.h>

#include <sstream>

geometry_msgs::Pose nav_goal_data;
int finish_navi = 0;



void hsr_nav_goal_status_Callback(const std_msgs::Int64& msg);
void nav_goal_send(geometry_msgs::Pose A);
void Nav_status_Dis();
void NAV_Goal_send(geometry_msgs::Pose A);
ros::Publisher hsr_nav_goal_pub;
ros::Subscriber hsr_nav_goal_status_sub;

geometry_msgs::Pose nav_goal_data;
int finish_navi = 0;
int state_navi = 0;
int check_navi = 0;
int seq=0;
int nodeFree = 0;

void nav_goal_send(geometry_msgs::Pose A)
{
  nav_goal_data.position.x = A.position.x;
  nav_goal_data.position.y = A.position.y;
  nav_goal_data.orientation.x = A.orientation.x;
  nav_goal_data.orientation.y = A.orientation.y;
  nav_goal_data.orientation.z = A.orientation.z;
  nav_goal_data.orientation.w = A.orientation.w;
  state_navi = 81;
  check_navi = 82;
  while(state_navi<82)
  {  
    printf("navi : %d  %d\n",state_navi,check_navi);
    switch(state_navi)
    {
      case 81 :{
        if(check_navi == 82)
        {
            hsr_nav_goal_pub.publish(nav_goal_data); 
            finish_navi =0;
            check_navi = 83;
        }
        if(finish_navi == 1 && check_navi == 83)
        {
            ros::Duration(5).sleep(); 
            state_navi = 82;
            check_navi = 84;
            break;
        }
      }
      case 82 :{

        break;
      }


    
    }
   ros::spinOnce(); 
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "main");


  ros::NodeHandle n;

 
  // //Publisher
  // hsr_nav_goal_pub = n.advertise<geometry_msgs::Pose>("/nav_goal", 1000);
  // //Subscriber
  // hsr_nav_goal_status_sub = n.subscribe("/navi_finish", 1000, hsr_nav_goal_status_Callback);
  //Publisher
  hsr_nav_goal_pub = n.advertise<geometry_msgs::Pose>("/hsr_nav_goal", 1000);
  //Subscriber
  hsr_nav_goal_status_sub = n.subscribe("/hsr_nav_goal_status", 1000, hsr_nav_goal_status_Callback);
  ros::Rate loop_rate(10);


  geometry_msgs::Pose M;
  M.position.x = 1.0;
  M.position.y = -1.0;
  M.orientation.w = 1.0;

  geometry_msgs::Pose N;
  N.position.x = 3.0;
  N.position.y = 0.0;
  N.orientation.w = 1.0;

  geometry_msgs::Pose O;
  O.position.x = 0.0;
  O.position.y = 0.0;
  O.orientation.w = 1.0;
  
  

  while (ros::ok())
  {
    // M.position.x = 1.0;
    // M.position.y = -1.0;
    // M.orientation.w = 1.0;
    // hsr_nav_goal_pub.publish(M);

    NAV_Goal_send(N);
    Nav_status_Dis();
    if(nav_goal_data_status_flag == 1){
      nav_goal_data_status_flag =0;
    }

    if(nav_goal_data_status_flag == 0 ){
      // system("x-terminal-emulator -e rosrun hsr_2018 simple_navigation_goals");
      if(seq == 0)
        NAV_Goal_send(M);
      else if(seq == 1)
        NAV_Goal_send(N);
      else if(seq == 2)
        NAV_Goal_send(O);
      seq++;
      nav_goal_data_status_flag = 1;
    }

    ros::spinOnce();

    loop_rate.sleep();
  }


  return 0;
}


// void NAV_Goal_send(geometry_msgs::Pose A){

//   nav_goal_data.position.x = A.position.x;
//   nav_goal_data.position.y = A.position.y;
//   nav_goal_data.orientation.x = A.orientation.x;
//   nav_goal_data.orientation.y = A.orientation.y;
//   nav_goal_data.orientation.z = A.orientation.z;
//   nav_goal_data.orientation.w = A.orientation.w;

//   // nav_goal_data.position.x = 1.0;
//   // nav_goal_data.position.y = -1.0;
//   // nav_goal_data.orientation.x = A.orientation.x;
//   // nav_goal_data.orientation.y = A.orientation.y;
//   // nav_goal_data.orientation.z = A.orientation.z;
//   // nav_goal_data.orientation.w = 1.0;

//   hsr_nav_goal_pub.publish(nav_goal_data);
// }

// int Robot_Nav(   ){
    
// }


void hsr_nav_goal_status_Callback(const std_msgs::Int64& msg)
{
  nav_goal_data_status_flag = msg.data;
}

// void Nav_status_Dis(){
//   // if(nav_goal_data_status_flag == 0){
//   //       ROS_INFO("Node : ""simple_navigation_goals"" is free ");
//   //   }
//   //   else if(nav_goal_data_status_flag == 1){
//   //       ROS_INFO("Node : ""simple_navigation_goals"" is done ");
//   //       nav_goal_data_status_flag = 0;
//   //       nodeFree = 0;
//   //   }
//   //   else if(nav_goal_data_status_flag == 2){
//   //       ROS_INFO("Node : ""simple_navigation_goals"" is busy ");
        
//   //   }
//   //   else if(nav_goal_data_status_flag == -1){
//   //       ROS_INFO("Node : ""simple_navigation_goals"" is failed ");
//   //       nav_goal_data_status_flag = 0;
//   //   }
//   //   else if(nav_goal_data_status_flag == -2){
//   //       ROS_INFO("Node : ""simple_navigation_goals"" is still right there ");
//   //       nav_goal_data_status_flag = 0;
//   //   }
//     ROS_INFO(" nav flag : %d seq : %d  nodeFree : %d ",nav_goal_data_status_flag,seq,nodeFree);

// }